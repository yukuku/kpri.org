<?php
class DB {
	public static $inst = null;
	
	public function instance() {
		if(DB::$inst == null) { 
			// connect to db
			
			// fetch credentials
			$cred = require_once dirname(__FILE__) . '/database-info.php';
			DB::$inst = mysql_connect($cred['server'], $cred['username'], $cred['password']);
			
			// select db and encoding from previous link
			mysql_select_db($cred['database']);
			
			// set group_concat_max_len
			mysql_query('set group_concat_max_len = ' . 5 * 1024); // 10MB
			
			// set character set
			mysql_query('set names utf8');
		}
		return DB::$inst;
	}
}
