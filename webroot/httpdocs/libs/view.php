<?php
class View {
	public $defaultTemplate = 'views/template.php';
	public $css, $js; // inline css and javascript respectively
	public $viewVars;
	
	function __construct($templateFile = null) {
		if(!empty($templateFile)) $this->defaultTemplate = 'views/template.php';
		$this->css = $this->js = $this->viewVars = array();
	}
	
	function render($contentFile) {
		// render inner content
		$content = null;
		extract($this->viewVars, EXTR_SKIP);
		if(file_exists(dirname(__FILE__) . '/views/' . $contentFile . '.php')) {
			ob_start();
			require_once 'views/' . $contentFile . '.php';
			$content = ob_get_clean();
		}
		
		$css = join("\n", $this->css);
		$js = join("\n", $this->js);

		// get list of songs for dropdown menu
		require_once 'db.php';	
		$db = DB::instance();
		$songs = array();
		if($db) {
			$rs = mysql_query('select no, judul from lagu');
			while($row = mysql_fetch_assoc($rs)) {
				$songs[$row['no'] . $this->slug($row['judul'])] = $row['no'] . '. ' . $row['judul'];
			}
		}
		
		
		// then outer
		require_once $this->defaultTemplate;
	}

	function ornament($type, $content) {
		switch($type) {
			case 'css': $this->css[] = '<style type="text/css">' . $content . '</style>'; break;
			case 'cssFile': $this->css[] = '<link rel="stylesheet" href="' . $content . '">'; break;
			case 'js': $this->js[] = '<script type="text/javascript">' . $content . '</script>'; break;
			case 'jsFile': $this->js[] = '<script type="text/javascript" src="' . $content . '"></script>'; break;
		}
	}
	
	/**
	 * Set data for view
	 * @param $key
	 * @param $val
	 */
	function s($key, $val) {
		$this->viewVars[$key] = $val;
	}

	/**
	 * Generate a slug from a string
	 * @param $str string to generate a slug from
	 */
	function slug($str) {
		$slug = preg_replace('/\s+/', '-', $str);
		$slug = preg_replace('/[^0-9a-zA-Z-]/', '', $slug);
		$slug = '/' . trim(strtolower($slug));
		return $slug;
	}
	
	/**
	 * Highlight a search result
	 * @param $str string of original content
	 * @param $keywordsArr array of keywords to apply highglighting to $str
	 */
	function highlightResult($str, $keywordsArr) {
		$strArr = explode("\n", $str);
		$highlighted = '';
		
		foreach($strArr as $line) {
			if(empty($line)) continue; 
			
			// flags to indicate whether a line has at least a matching keyword and 
			// whether it's the first line respectively
			$gotMatch = $firstLine = false;
			
			// test each keyword against a line and append to result
			foreach($keywordsArr as $i => $k) {
				if(stripos($line, $k) !== false) {
					$gotMatch = true;
					$line = str_ireplace($k, '{' . $k . '}', $line);
					
					$prefix = '';
				}
			}
			
			if($gotMatch) {
				$highlighted .= $prefix . $line . "\n";
			}
		}
		
		// replace { and } with <span>'s, we didn't do this earlier to avoid <span> getting replaced
		// in the next keyword
		
		$highlighted = str_replace('{', '<span class="h">', $highlighted);
		$highlighted = str_replace('}', '</span>', $highlighted);
		
		return '... ' . rtrim($highlighted) . ' ... ';
	}
}
