<?php if(!empty($songContent)): ?> 
<?php $songData = $songContent[0]; ?>
<div id="singleSong">
	<h1 class="songTitle_big">
		<?php echo $songData['no'] ?>.
		<?php echo $songData['judul']; ?>
	</h1>
	<div class="songInfo">
		<div class="songPeople">
			<div class="songLyricist"><?php echo $songData['xpengarang_lirik']; ?></div>
			<div class="songComposer"><?php echo $songData['xpengarang_musik']; ?></div>
			<div class="clear"></div>
		</div>
		<div class="songTech">
			<?php echo !empty($songData['birama']) ? $songData['birama'] . ', ' : ''; ?>
			<?php echo $songData['nadaDasar'] ?>
		</div>
	</div>
	<div class="songLyric">
	<?php $firstVerse = $contentHtml = $reffHtml = array(); ?>
	<?php foreach($songContent as $c): ?>
		<?php $lirikId = $c['lirik_id']; ?>
		<?php if($c['reff'] == 1): ?>
			<?php 
			if(empty($reffHtml[$lirikId])) {
				$reffHtml[$lirikId] = '';
			}
			$reffHtml[$lirikId] .= '<div class="songLine">' . nl2br($c['xbaris']) . '</div>'; ?>
		<?php else: ?>
			<?php 
			if(empty($contentHtml[$lirikId])) $contentHtml[$lirikId] = '';
			$contentHtml[$lirikId] = $contentHtml[$lirikId] . 
			'<div class="songLine">' . 
				'<div class="songNumber">' . $c['urutan'] . '</div>' .
				'<div class="songContent">' .nl2br($c['xbaris']) . '<br/></div>' .
				'<div class="clear"></div>' .
			'</div>';

			if(empty($firstVerse[$lirikId])) {
				$contentHtml[$lirikId] .= '%s';
				$firstVerse[$lirikId] = true;
			}
			?>
		<?php endif; ?>	
	<?php endforeach; ?>
	<?php
	foreach($contentHtml as $i => $content) {
		// if reff exist, wrap
		if(!empty($reffHtml[$i])) {
			$reffHtml[$i] = '<div class="songReff"><div class="songNumber">ref:</div><div class="songContent">' . $reffHtml[$i] . '</div><div class="clear"></div></div>';
		} else {
			$reffHtml[$i] = '';
		}
		printf('<div class="lyricWrapper">' . $content . '</div>', $reffHtml[$i]);
	}
	?>
	</div>
</div>
<?php endif; ?>
