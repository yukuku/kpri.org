<?php if(!empty($results)): ?>
<div id="searchResultsWrapper">
<?php foreach($results as $i => $song): ?>
	<div class="searchResultWrapper">
		<div class="songTitle">
			<span class="songNumber"><?php echo $song['no'] ?>.</span>
			<?php $href = '/lagu/' . $song['no'] . $this->slug($song['judul']); ?>
			<a title="<?php echo $song['no'] . '. ' . $song['judul'] ?>" href="<?php echo $href ?>"><?php echo $song['judul'] ?></a>
		</div>
		<div class="songContent">
			<?php echo nl2br($this->highlightResult($song['isi'], $keywords)); ?>
		</div>
	</div>
<?php endforeach; ?>
</div>
<?php else: ?>
<br/>
<span style="color:#FF6543">Tidak ada lagu yang sesuai dengan kriteria pencarian anda.</span>

<?php endif; ?>
