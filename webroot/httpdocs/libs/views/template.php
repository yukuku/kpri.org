<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>KPRI.org</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="/css/main.css">
<?php echo $css ?>

<?php if(!empty($_SERVER['HTTP_HOST']) && strpos($_SERVER['HTTP_HOST'], 'kpri.org') !== false): ?>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-30198782-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<?php endif;?>
</head>
<body>
	<div id="outer_header">
		<div id="header">
			<h3><a href="/">KPRI.org</a></h3>
			<div id="tagline">website kumpulan kidung rohani pilihan</div>
		</div>
		<div class="clear"></div>
	</div>
	<div id="container">
		<div id="content">
			<div id="songJumper">
				<select id="songSelect">
					<option value="">-- Pilih Lagu --</option>
				<?php foreach($songs as $i => $title): ?>
					<option value="<?php echo $i; ?>"><?php echo $title; ?></option>
				<?php endforeach; ?>
				</select>
				<form id="songNumberInput" action="index.php" method="get">
					Lompat ke nomor: <input type="text" name="no" id="songNo" />
				</form>
				<form id="searchForm" action="/search" method="get">
					Cari <select id="qType" name="f"><option value="i">Isi Lagu</option><option value="j">Judul Lagu</option></select>
					<input type="text" name="q" id="qText" value="<?php echo !empty($keywords) ? join(" ", $keywords) : ""; ?>"/>
					<input type="submit" value="Cari" />
				</form>
			</div>
			<?php echo $content ?>
		</div>
		<div id="footer"></div>
	</div>
	<script type="text/javascript">
	var chooseSong = document.getElementById('songSelect');
	chooseSong.onchange = function(evt) {
		window.location.href = '/lagu/' + this.value;
	}

	<?php if(!empty($no)): ?>
	var no = "<?php echo $no ?>";
	chooseSong.value = no;
	<?php endif; ?>
	
	<?php if(!empty($qType)): ?>
	document.getElementById('qType').value = '<?php echo $qType; ?>';
	<?php endif; ?>

	document.getElementById('songNumberInput').onsubmit = function(e) {
		chooseSong.selectedIndex = document.getElementById('songNo').value;
		chooseSong.onchange();
		e.preventDefault();
		return false;
	};

	<?php if(!empty($_GET['offline'])): ?>
	
	var newScr = document.createElement("script"); 
	newScr.src = "/js/jq.js"; 
	newScr.type="text/javascript"; 
	document.getElementsByTagName("head")[0].appendChild(newScr); 
	<?php endif; ?>
	
	</script>
	<?php echo $js ?>
	
</body>
</html>
