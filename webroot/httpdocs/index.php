<?php
require_once 'libs/view.php';
require_once 'libs/db.php';

$v = new View();

if(!empty($_GET['no']) && preg_match('/^\d+/', $_GET['no'])) {
	$db = DB::instance();
	$rs = mysql_query(
		'SELECT * FROM lagu, lirik, bait ' .
		'WHERE lagu.id = lirik.lagu_id AND bait.lirik_id = lirik.id AND lagu.no = ' . $_GET['no']
	, $db);
	
	$toSet = array();
	while($l = mysql_fetch_assoc($rs)) {
		$toSet[] = $l;
	}
	$v->s('no', $_GET['no'] . $v->slug($toSet[0]['judul']));
	$v->s('songContent', $toSet);
}

$v->render('index');
