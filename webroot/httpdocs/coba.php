<?php
require_once 'libs/view.php';
require_once 'libs/db.php';

$v = new View();

if(!empty($_GET['no']) && preg_match('/^\d+/', $_GET['no'])) {
	$db = DB::instance();
	$rs = mysql_query(
		'SELECT * FROM lagu, lirik, bait ' .
		'WHERE lagu.id = lirik.lagu_id AND bait.lirik_id = lirik.id AND lagu.no = ' . $_GET['no']
	, $db);
	
	$toSet = array();
	while($l = mysql_fetch_assoc($rs)) {
		$toSet[] = $l;
	}
	
	$songContent = $toSet;
	$songData = $toSet[0];
}

ob_start();
?>
<h1 class="songTitle_big">
	<?php echo $songData['no'] ?>.
	<?php echo $songData['judul']; ?>
</h1>
<div class="songInfo">
	<div class="songPeople">
		<div class="songLyricist"><?php echo $songData['xpengarang_lirik']; ?></div>
		<div class="songComposer"><?php echo $songData['xpengarang_musik']; ?></div>
		<div class="clear"></div>
	</div>
	<div class="songTech">
		<?php echo !empty($songData['birama']) ? $songData['birama'] . ', ' : ''; ?>
		<?php echo $songData['nadaDasar'] ?>
	</div>
</div>
<div class="songLyric">
<?php $firstVerse = $contentHtml = $reffHtml = array(); ?>
<?php foreach($songContent as $c): ?>
	<?php $lirikId = $c['lirik_id']; ?>
	<?php if($c['reff'] == 1): ?>
		<?php 
		if(empty($reffHtml[$lirikId])) {
			$reffHtml[$lirikId] = '';
		}
		$reffHtml[$lirikId] .= '<div class="songLine">' . nl2br($c['xbaris']) . '</div>'; ?>
	<?php else: ?>
		<?php 
		if(empty($contentHtml[$lirikId])) $contentHtml[$lirikId] = '';
		$contentHtml[$lirikId] = $contentHtml[$lirikId] . 
		'<div class="songLine">' . 
			'<div class="songNumber">' . $c['urutan'] . '</div>' .
			'<div class="songContent">' .nl2br($c['xbaris']) . '<br/></div>' .
			'<div class="clear"></div>' .
		'</div>';

		if(empty($firstVerse[$lirikId])) {
			$contentHtml[$lirikId] .= '%s';
			$firstVerse[$lirikId] = true;
		}
		?>
	<?php endif; ?>	
<?php endforeach; ?>
<?php
foreach($contentHtml as $i => $content) {
	// if reff exist, wrap
	if(!empty($reffHtml[$i])) {
		$reffHtml[$i] = '<div class="songReff"><div class="songNumber">ref:</div><div class="songContent">' . $reffHtml[$i] . '</div><div class="clear"></div></div>';
	} else {
		$reffHtml[$i] = '';
	}
	printf('<div class="lyricWrapper">' . $content . '</div>', $reffHtml[$i]);
}
?>
</div>
<?php 
ob_end_flush();
?>