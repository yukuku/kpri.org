<?php
require_once 'libs/view.php';
require_once 'libs/db.php';

if(!empty($_GET['q'])) {
	$v = new View();
	$q = $_GET['q'];
	
	DB::instance();
	
	// take out all special characters except comma and dot
	$q = preg_replace('/[^a-zA-Z ,.]/', '', $q);
	
	
	// compress whitespaces
	$q = preg_replace('/ +/', ' ', $q);
	$q = trim($q);
	$v->s('keywords', explode(' ', $q));
	
	// insert fulltext syntax searches
	$q = '+' . preg_replace('/\s+/', '* +', $q) . '*';

	$fieldToMatch = 'xbaris';
	if(!empty($_GET['f'])) {
		$field = $_GET['f'];
		if(in_array($field, array('i', 'j'))) {
			$v->s('qType', $field);
			switch($field) {
				case 'i': $fieldToMatch = 'xbaris'; break;
				case 'j': $fieldToMatch = 'judul'; break;
			}
		}
	}
	
	$rs = mysql_query(
		'SELECT no, judul, xpengarang_lirik, xpengarang_musik, nadaDasar, birama, ' .
		'group_concat(xbaris SEPARATOR \'\n\n\') as isi FROM lagu, lirik, bait ' .
		'WHERE lagu.id = lirik.lagu_id AND bait.lirik_id = lirik.id AND ' .
		'match(' . $fieldToMatch . ') against (\'' . $q . '\' in boolean mode) ' .
		'group by lagu_id ' .  
		'order by judul limit 200 '
	);
	
	$results = array();
	while($row = mysql_fetch_assoc($rs)) {
		$results[] = $row;
	}

	$v->s('results', $results);
	$v->render('search');
} else {
	header("Location: /");
}
